package ru.t1.strelcov.tm.listener;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.t1.strelcov.tm.api.ILoggerService;

import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.TextMessage;

@Component
public class LoggerListener implements MessageListener {

    @Autowired
    @NotNull
    private ILoggerService loggerService;

    @Override
    @SneakyThrows
    public void onMessage(@NotNull final Message message) {
        if (!(message instanceof TextMessage)) return;
        @NotNull final String json = ((TextMessage) message).getText();
        loggerService.writeLog(json);
    }

}
