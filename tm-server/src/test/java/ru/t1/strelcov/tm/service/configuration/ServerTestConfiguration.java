package ru.t1.strelcov.tm.service.configuration;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.FilterType;
import org.springframework.context.annotation.Scope;
import ru.t1.strelcov.tm.api.service.IPropertyService;
import ru.t1.strelcov.tm.bootstrap.Bootstrap;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.util.logging.Level;
import java.util.logging.Logger;

@ComponentScan(basePackages = {"ru.t1.strelcov.tm"},
        excludeFilters = {
                @ComponentScan.Filter(type = FilterType.ASSIGNABLE_TYPE, value = Bootstrap.class)})
public class ServerTestConfiguration {

    @Bean(destroyMethod = "close")
    public EntityManagerFactory entityManagerFactory(@NotNull final IPropertyService propertyService) {
        Logger.getLogger("org.hibernate").setLevel(Level.parse(propertyService.getHibernateLogLevel()));
        return Persistence.createEntityManagerFactory(propertyService.getPersistenceUnit());
    }

    @Scope("prototype")
    @Bean
    public EntityManager entityManager(@NotNull final EntityManagerFactory entityManagerFactory) {
        return entityManagerFactory.createEntityManager();
    }

}
