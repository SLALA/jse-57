package ru.t1.strelcov.tm.configuration;

import lombok.SneakyThrows;
import org.apache.activemq.broker.BrokerService;
import org.apache.log4j.BasicConfigurator;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Scope;
import ru.t1.strelcov.tm.api.service.IPropertyService;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.util.logging.Level;
import java.util.logging.Logger;

@ComponentScan("ru.t1.strelcov.tm")
public class ServerConfiguration {

    @Bean(destroyMethod = "close")
    public EntityManagerFactory entityManagerFactory(@NotNull final IPropertyService propertyService) {
        Logger.getLogger("org.hibernate").setLevel(Level.parse(propertyService.getHibernateLogLevel()));
        return Persistence.createEntityManagerFactory(propertyService.getPersistenceUnit());
    }

    @Scope("prototype")
    @Bean
    public EntityManager entityManager(@NotNull final EntityManagerFactory entityManagerFactory) {
        return entityManagerFactory.createEntityManager();
    }

    @SneakyThrows
    @Bean(initMethod = "start", destroyMethod = "stop")
    public BrokerService broker(@NotNull final IPropertyService propertyService) {
        BasicConfigurator.configure();
        @NotNull final BrokerService broker = new BrokerService();
        broker.addConnector(propertyService.getMQConnectionFactory());
        return broker;
    }

}
