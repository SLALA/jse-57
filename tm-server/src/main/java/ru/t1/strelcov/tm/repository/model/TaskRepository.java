package ru.t1.strelcov.tm.repository.model;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.t1.strelcov.tm.api.repository.model.ITaskRepository;
import ru.t1.strelcov.tm.model.Task;

import javax.persistence.EntityManager;
import java.util.List;

@Repository
@Scope("prototype")
public final class TaskRepository extends AbstractBusinessRepository<Task> implements ITaskRepository {

    @Autowired
    public TaskRepository(@NotNull final EntityManager entityManager) {
        super(entityManager);
    }

    @Override
    @NotNull
    public Class<Task> getClazz() {
        return Task.class;
    }

    @Override
    public void removeAllByProjectId(@NotNull final String userId, @NotNull final String projectId) {
        @NotNull final String jpql = "DELETE FROM " + getEntityName() + " e WHERE e.user.id = :userId AND e.projectId = :projectId";
        entityManager.createQuery(jpql).setParameter("userId", userId).setParameter("projectId", projectId).executeUpdate();
    }

    @SneakyThrows
    @NotNull
    @Override
    public List<Task> findAllByProjectId(@NotNull final String userId, @NotNull final String projectId) {
        @NotNull final String jpql = "SELECT e FROM " + getEntityName() + " e WHERE e.user.id = :userId AND e.project.id = :projectId";
        return entityManager.createQuery(jpql, getClazz()).setParameter("userId", userId).setParameter("projectId", projectId).getResultList();
    }

}
