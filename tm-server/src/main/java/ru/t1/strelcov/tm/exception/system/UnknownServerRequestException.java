package ru.t1.strelcov.tm.exception.system;

import org.jetbrains.annotations.Nullable;
import ru.t1.strelcov.tm.exception.AbstractException;

public final class UnknownServerRequestException extends AbstractException {

    public UnknownServerRequestException(@Nullable final String value) {
        super("Error: Unknown server request \"" + value + "\".");
    }

}
