package ru.t1.strelcov.tm.endpoint;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import ru.t1.strelcov.tm.api.endpoint.IAuthEndpoint;
import ru.t1.strelcov.tm.dto.model.SessionDTO;
import ru.t1.strelcov.tm.dto.model.UserDTO;
import ru.t1.strelcov.tm.dto.request.UserLoginRequest;
import ru.t1.strelcov.tm.dto.request.UserLogoutRequest;
import ru.t1.strelcov.tm.dto.request.UserProfileRequest;
import ru.t1.strelcov.tm.dto.response.UserLoginResponse;
import ru.t1.strelcov.tm.dto.response.UserLogoutResponse;
import ru.t1.strelcov.tm.dto.response.UserProfileResponse;
import ru.t1.strelcov.tm.service.dto.UserDTOService;

import javax.jws.WebService;

@Controller
@WebService(endpointInterface = "ru.t1.strelcov.tm.api.endpoint.IAuthEndpoint")
@NoArgsConstructor
public final class AuthEndpoint extends AbstractEndpoint implements IAuthEndpoint {

    @Autowired
    @NotNull
    private UserDTOService userDTOService;

    @NotNull
    @Override
    public UserLoginResponse login(@NotNull final UserLoginRequest request) {
        return new UserLoginResponse(authDTOService.login(request.getLogin(), request.getPassword()));
    }

    @NotNull
    @Override
    public UserLogoutResponse logout(@NotNull final UserLogoutRequest request) {
        return new UserLogoutResponse();
    }

    @NotNull
    @Override
    public UserProfileResponse getProfile(@NotNull final UserProfileRequest request) {
        @NotNull final SessionDTO session = check(request);
        @NotNull final UserDTO user = userDTOService.findById(session.getUserId());
        return new UserProfileResponse(user);
    }

}
