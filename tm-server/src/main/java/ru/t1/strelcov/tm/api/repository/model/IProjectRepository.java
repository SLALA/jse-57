package ru.t1.strelcov.tm.api.repository.model;

import ru.t1.strelcov.tm.model.Project;

public interface IProjectRepository extends IBusinessRepository<Project> {

}
