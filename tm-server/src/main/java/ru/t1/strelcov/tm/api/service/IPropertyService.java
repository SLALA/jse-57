package ru.t1.strelcov.tm.api.service;

public interface IPropertyService {

    Integer getPasswordIteration();

    String getPasswordSecret();

    String getName();

    String getVersion();

    String getEmail();

    String getServerHost();

    Integer getServerPort();

    String getSessionSecret();

    Integer getSessionTimeout();

    String getBackupStatus();

    String getInitDataStatus();

    String getHibernateLogLevel();

    String getPersistenceUnit();

    String getMQConnectionFactory();
}
