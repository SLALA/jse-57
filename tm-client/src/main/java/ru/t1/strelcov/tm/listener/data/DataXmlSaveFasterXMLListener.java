package ru.t1.strelcov.tm.listener.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.strelcov.tm.dto.request.DataXmlFasterXMLSaveRequest;
import ru.t1.strelcov.tm.enumerated.Role;
import ru.t1.strelcov.tm.event.ConsoleEvent;

@Component
public final class DataXmlSaveFasterXMLListener extends AbstractDataListener {

    @Override
    @Nullable
    public String arg() {
        return null;
    }

    @Override
    @NotNull
    public String name() {
        return "data-xml-save-fasterxml";
    }

    @Override
    @NotNull
    public String description() {
        return "Save entities data to xml file.";
    }

    @Override
    @SneakyThrows
    @EventListener(condition = "@dataXmlSaveFasterXMLListener.name() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        System.out.println("[DATA XML SAVE]");
        dataEndpoint.saveXmlFasterXMLData(new DataXmlFasterXMLSaveRequest(getToken()));
    }

    @Nullable
    @Override
    public Role[] roles() {
        return new Role[]{
                Role.ADMIN
        };
    }

}
