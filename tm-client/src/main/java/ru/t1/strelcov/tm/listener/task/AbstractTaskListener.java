package ru.t1.strelcov.tm.listener.task;

import org.jetbrains.annotations.Nullable;
import ru.t1.strelcov.tm.dto.model.TaskDTO;
import ru.t1.strelcov.tm.listener.AbstractListener;

public abstract class AbstractTaskListener extends AbstractListener {

    protected void showTask(@Nullable final TaskDTO task) {
        if (task == null) return;
        System.out.println("[Id]: " + task.getId());
        System.out.println("[Name]: " + task.getName());
        System.out.println("[Description]: " + task.getDescription());
        System.out.println("[Status]: " + task.getStatus().getDisplayName());
        System.out.println("[Project Id]: " + task.getProjectId());
        System.out.println("[User Id]: " + task.getUserId());
    }

}
